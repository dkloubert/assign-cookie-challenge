import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.*
import java.util.stream.Stream

class AssignCookiesTest {

    @ParameterizedTest(name = "it counts the satisfied children for {arguments}")
    @MethodSource("testData")
    fun `it counts the satisfied children`(
        greedFactors: IntArray,
        cookieSizes: IntArray,
        expectedResult: Int
    ) {
        val result = findContentChildren(greedFactors, cookieSizes)

        assertThat(result).isEqualTo(expectedResult)
    }

    companion object {
        @JvmStatic
        fun testData(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(intArrayOf(1,2,3), intArrayOf(1,1), 1),
                Arguments.of(intArrayOf(1,2), intArrayOf(1,2,3), 2),
                Arguments.of(intArrayOf(4,1,3,2,1,1,2,2), intArrayOf(1,1,1,1), 3),
                Arguments.of(intArrayOf(4,1,3,2,1,1,2,2), intArrayOf(2,2,2,2), 4),
                Arguments.of(intArrayOf(), intArrayOf(), 0),
                Arguments.of(intArrayOf(1,1), intArrayOf(), 0),
                Arguments.of(intArrayOf(), intArrayOf(1,1), 0)
            )
        }
    }
}