fun findContentChildren(g: IntArray, s: IntArray): Int {
    val greedFactors = g.sortedArrayDescending()
    val cookieSizes = s.sortedArrayDescending().toMutableList()
    return greedFactors.fold(0) {acc, greedFactor ->
        cookieSizes.firstOrNull { it >= greedFactor }?.let {
            cookieSizes.remove(it)
            acc + 1
        } ?: acc
    }
}